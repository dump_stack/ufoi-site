/* eslint-disable @typescript-eslint/no-var-requires */
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const lightCodeTheme = require('prism-react-renderer/themes/github');

// With JSDoc @type annotations, IDEs can provide config autocompletion
/** @type {import('@docusaurus/types').DocusaurusConfig} */
(
  module.exports = {
    title: 'United Federation of Instances',
    tagline:
      'A federation of good-faith actors on the Fediverse',
    url: 'https://UFoI.org',
    baseUrl: '/',
    favicon: 'img/favicon.ico',
    organizationName: 'UFoI',
    projectName: 'UFoI',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'throw',
    presets: [
      [
        '@docusaurus/preset-classic',
        /** @type {import('@docusaurus/preset-classic').Options} */
        ({
          docs: {
            path: 'docs',
            sidebarPath: 'sidebars.js',
            editUrl:
              'https://gitlab.com/ufoi/ufoi-site',
            versions: {
              current: {
                label: 'current',
              },
            },
            lastVersion: 'current',
            showLastUpdateAuthor: true,
            showLastUpdateTime: true,
          },
          theme: {
            customCss: require.resolve('./src/css/custom.css'),
          },
        }),
      ],
    ],
    plugins: ['my-loaders', 'tailwind-loader'],
    themeConfig:
      /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
      ({
        algolia: {
          appId: 'R2IYF7ETH7',
          apiKey: '599cec31baffa4868cae4e79f180729b',
          indexName: 'docsearch',
          contextualSearch: true,
        },
        navbar: {
          hideOnScroll: true,
          logo: {
            alt: 'United Federation of Instances',
            src: 'img/logo-text-small.png',
            srcDark: 'img/logo-white-text-small.png',
          },
          items: [
            // left
            {
              label: 'Docs',
              to: 'docs/getting-started',
              position: 'left',
            },
            // right
            {
              type: 'docsVersionDropdown',
              position: 'right',
            },
            {
              href: 'https://gitlab.com/ufoi',
              position: 'right',
              className: 'header-github-link',
            },
          ],
        },
        colorMode: {
          defaultMode: 'light',
          disableSwitch: false,
          respectPrefersColorScheme: true,
        },
        announcementBar: {
          content:
            '⭐️ If you like the UFoI, give it a star on <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/ufoi/constitution">GitLab</a>! ⭐️',
        },
        footer: {
          links: [
            {
              title: 'About',
              items: [
                {
                  label: 'Getting Started',
                  to: 'docs/getting-started',
                },
                {
                  label: 'FAQ',
                  to: 'docs/faq',
                },
                {
                  label: 'Proposal',
                  to: 'https://ufoi.gitlab.io/constitution/united_federation_of_instances_proposal.pdf',
                },
                {
                  label: 'Bylaws',
                  to: 'https://ufoi.gitlab.io/constitution/united_federation_of_instances_bylaws.pdf',
                },
              ],
            },
            {
              title: 'Governance',
              items: [
                {
                  label: 'Apply',
                  to: 'apply',
                },
                {
                  label: 'Report an Instance',
                  to: 'https://gitlab.com/-/ide/project/ufoi/reports/edit/master/-/REPORTS.md',
                },
                {
                  label: 'Suggest an Ammendment',
                  to: 'https://gitlab.com/-/ide/project/ufoi/constitution/edit/master/-/constitution.tex',
                },
              ],
            },
            {
              title: 'Community',
              items: [
                {
                  label: 'Matrix Chat',
                  to: 'https://matrix.to/#/#UFoI:matrix.org',
                },
                {
                  label: 'Fediverse Group',
                  to: 'https://a.gup.pe/u/ufoi',
                },
                {
                  label: 'GitLab',
                  to: 'https://gitlab.com/ufoi/',
                },
              ],
            },
            {
              title: 'Resources',
              items: [
                {
                  label: 'Instance Members',
                  to: 'docs/instance-members',
                },
                {
                  label: 'Council Members',
                  to: 'docs/council-members',
                },
                {
                  label: 'Coalitions',
                  to: 'docs/coalitions',
                },
                {
                  label: 'Sister Federations',
                  to: 'docs/sister-federations',
                },
              ],
            },
          ],
          logo: {
            alt: 'Algolia',
            src: 'img/logo-text.png',
            srcDark: 'img/logo-white-text.png',
            width: 200,
          },
          copyright: 'Copyright UFoI 2022 - present',
        },
        image: 'img/logo-text.png',
        prism: {
          theme: lightCodeTheme,
          darkTheme: darkCodeTheme,
        },
      }),
  }
);
