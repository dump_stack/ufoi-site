import {
  Button,
  Card,
  Heading1,
  InlineLink,
  Input,
  LabelText,
  Text,
} from '@algolia/ui-library';
import { useBaseUrlUtils } from '@docusaurus/useBaseUrl';
import React, { useState } from 'react';

function ApplyForm() {
  const { withBaseUrl } = useBaseUrlUtils();
  const [state, setState] = useState({ status: 'stalled', message: '' });
  const [url, setUrl] = useState('');
  const [email, setEmail] = useState('');
  const [repo, setRepo] = useState('');

  const handleSetUrl = (event) => {
    setUrl(event.target.value);
  };
  const handleSetEmail = (event) => {
    setEmail(event.target.value);
  };
  const handleSetRepo = (event) => {
    setRepo(event.target.value);
  };
  const onSubmit = (event) => {
    event.preventDefault();

    if (state.status === 'loading') {
      return;
    }

    setState({ status: 'loading' });

    const applyForm = event.target;
    const method = applyForm.getAttribute('method');
    const action = applyForm.getAttribute('action');
    const formData = new FormData(applyForm);
    const data = {};
    formData.forEach(function (value, key) {
      data[key] = value;
    });
    const body = JSON.stringify(data);

    fetch(action, {
      method,
      headers: { 'Content-Type': 'application/json' },
      body,
    })
      .then((response) => response.json())
      .then(({ success, message }) => {
        if (!success) {
          return setState({
            status: 'failed',
            message: 'Unable to submit your request.',
          });
        }

        return setState({ status: 'succeed', message });
      })
      .catch(() =>
        setState({
          status: 'failed',
          message: 'Unable to submit your request.',
        })
      );
  };

  if (state.status === 'succeed' && state.message) {
    return (
      <Card className="uil-m-auto uil-ta-center apply-form">
        <Heading1 className="apply-text">Thank you!</Heading1>
        <br />

        {state.message.startsWith('Your DocSearch') ? (
          <Text
            className="uil-pv-8 uil-d-block apply-text"
            aria-label="Request has already been processed"
          >
            {state.message}
          </Text>
        ) : (
          <>
            <Text
              className="uil-pv-8 uil-d-block apply-text"
              aria-label="Request will be processed"
            >
              {state.message} We'll get back to you at <strong>{email}</strong>{' '}
              with the snippet you'll need to integrate into{' '}
              <InlineLink href={url}>{url}</InlineLink>.
            </Text>

            <Text aria-label="recommendations" className="apply-text">
              Please be patient, in the meantime, you can implement{' '}
              <InlineLink href={withBaseUrl('docs/tips')}>
                our recommendations for building a great DocSearch experience.
              </InlineLink>
            </Text>
          </>
        )}
      </Card>
    );
  }

  return (
    <Card className="uil-m-auto apply-form uil-pb-24">
      <form
        id="form-apply-docsearch"
        method="POST"
        action="https://docsearch-hub.herokuapp.com/form/inbound"
        onSubmit={onSubmit}
      >
        <LabelText key="url" tag="label" htmlFor="url" className="apply-text">
          Instance url
          <Input
            required={true}
            id="url"
            type="url"
            name="url"
            aria-label="URL of the open-source blog or documentation website"
            value={url}
            placeholder="https://project.org/docs"
            onChange={handleSetUrl}
          />
        </LabelText>

        <Text small={true} className="uil-pv-8 uil-d-block apply-text">
          You must be the admin at this instance
        </Text>

        <LabelText
          tag="label"
          htmlFor="email"
          key="email"
          className="apply-text"
        >
          Email
          <Input
            required={true}
            id="email"
            type="email"
            name="email"
            aria-label="Email address of the admin of this instance"
            value={email}
            placeholder="you@domain.org"
            onChange={handleSetEmail}
          />
        </LabelText>

        <Text small={true} className="uil-pv-8 uil-d-block apply-text">
          We will e-mail you here to verify you're identity
        </Text>

        <LabelText
          tag="label"
          htmlFor="fedihandle"
          key="fedihandle"
          className="apply-text"
        >
          Fediverse handle
          <Input
            required={true}
            id="fedihandle"
            type="email"
            name="fedihandle"
            aria-label="The fediverse handle of the instance admin"
            value={email}
            placeholder="@you@instance.org"
            onChange={handleSetEmail}
          />
        </LabelText>

        <Text small={true} className="uil-pv-8 uil-d-block apply-text">
           We will send you a message here to verify it is really you
        </Text>

        <div className="uil-ph-32 uil-d-flex uil-fxd-column">
          <LabelText
            className="uil-pt-12 apply-text"
            tag="label"
            htmlFor="public"
            key="public"
          >
            <input
              required={true}
              id="public"
              name="public"
              aria-label="Confirm my instance is online"
              type="checkbox"
              className="uil-mr-8"
            />
            My instance is online
          </LabelText>

          <LabelText
            className="uil-pt-12 apply-text"
            tag="label"
            htmlFor="opensource"
            key="opensource"
          >
            <input
              required={true}
              id="opensource"
              name="opensource"
              aria-label="Confirm my website is a Fediverse instance"
              type="checkbox"
              className="uil-mr-8"
            />
            My website is a fediverse instance
          </LabelText>

          <LabelText
            className="uil-pt-12 apply-text"
            tag="label"
            htmlFor="owner"
            key="owner"
          >
            <input
              required={true}
              id="owner"
              name="owner"
              aria-label="Confirm I am the administrator of the instance"
              type="checkbox"
              className="uil-mr-8"
            />
            I'm the administrator of the instance and I have{' '}
            <InlineLink href={withBaseUrl('docs/who-can-apply')}>
              read the requirements.
            </InlineLink>
          </LabelText>

          <Button
            primary={true}
            disabled={state.status === 'loading'}
            className="uil-mt-16 uil-mb-16"
            tag="button"
            type="submit"
            id="joinButton"
          >
            Apply to the UFoI
          </Button>
        </div>

        <Text small={true} className="uil-ta-center">
          <strong>
            Only apply if you don't have a pending application.
          </strong>{' '}
        </Text>
      </form>
    </Card>
  );
}

export default ApplyForm;
