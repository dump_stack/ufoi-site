import {
  Button,
  Card,
  Heading1,
  Heading2,
  InlineLink,
  Input,
  LabelText,
  Text,
} from '@algolia/ui-library';
import { useBaseUrlUtils } from '@docusaurus/useBaseUrl';
import React, { useState } from 'react';
import instanceMembers from './instance-members.json';
import userMembers from './user-members.json';

function UserMember(props) {
  const { withBaseUrl } = useBaseUrlUtils();

  return (
    <div>
      {userMembers.map(({ name, display_name, profiles, pgp_fingerprint, instance_member_reps, on_council, on_ufoi_instance }) => name === props.name && (
        <Card className="uil-m-auto apply-form text-left">
          <Heading2 className="apply-text">{display_name}</Heading2>
          <br />
          { on_council ? (
            <div>Council Member: true</div>
          ) : (
            <div>Council Member: false</div>
          )}
          { on_ufoi_instance ? (
            <div>UFoI Instance Member: true</div>
          ) : (
            <div>UFoI Instance Member: false</div>
          )}
          {profiles.map(profile => (
            <div>
              Profile:
              <a
                href={profile.url}
                rel="me"
                target="_blank"
                alt={`User's profile link`}
                >
                  {profile.handle}
              </a>
              <br />
            </div>
          ))}
          { instance_member_reps && (
            <div>
              {instance_member_reps.map(instance_member_rep => (
                <div>
                  Instance Member Representation: {instance_member_rep}
                </div>            
              ))}
            </div>
          )}
        </Card>
      ))}
    </div>
  );
}

export default UserMember;
