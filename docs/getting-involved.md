---
title: Getting Involved
---

here are some alternate "official" ways the conversation should continue:

1) On the official Gitlab repository, which is where all voting and formal discussions will take place

https://gitlab.com/ufoi/constitution

2) The group on the fediverse (just tag them instead of everyone): [@ufoi@a.gup.pe](https://a.gup.pe/u/ufoi)

3) On matrix chat. [#UFoI:matrix.org](https://matrix.to/#/#UFoI:matrix.org)

And of sourse you are all welcome to contact me directly. I am very excited how there is so much talk about it and it hasnt even been released for 24 hours yet!
