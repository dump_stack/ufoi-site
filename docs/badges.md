---
title: Badges
---

We can use the verification process for links which already exists in Mastodon as a way of safely identifying instances. In short we would provide a list of links to each admin of each instance that is a member. Those admins would also link us and get the green check mark showing as verified. We can do something similar if we want to maintain badges and a central list for moderators too. This will make for an easy way to identify members of the UFI at a glance once we get larger.

